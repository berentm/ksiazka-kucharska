\babel@toc {polish}{}
\contentsline {section}{\numberline {1}Dania obiadowe}{1}% 
\contentsline {subsection}{\numberline {1.1}Tortilla}{1}% 
\contentsline {paragraph}{Składniki:}{1}% 
\contentsline {paragraph}{Przygotowanie}{1}% 
\contentsline {paragraph}{Porada}{1}% 
\contentsline {subsection}{\numberline {1.2}Pancakes}{2}% 
\contentsline {paragraph}{Składniki:}{2}% 
\contentsline {paragraph}{Przygotowanie}{2}% 
\contentsline {subsection}{\numberline {1.3}Naleśniki}{3}% 
\contentsline {paragraph}{Składniki:}{3}% 
\contentsline {paragraph}{Przygotowanie}{3}% 
\contentsline {subsection}{\numberline {1.4}Roladki nadziewane pesto z dodatkiem suszonych pomidorów}{4}% 
\contentsline {paragraph}{Składniki:}{4}% 
\contentsline {paragraph}{Przygotowanie}{4}% 
\contentsline {subsection}{\numberline {1.5}Kebab}{5}% 
\contentsline {paragraph}{Składniki:}{5}% 
\contentsline {paragraph}{Przygotowanie}{5}% 
\contentsline {subsection}{\numberline {1.6}Polędwiczki w sosie własnym}{7}% 
\contentsline {paragraph}{Składniki:}{7}% 
\contentsline {paragraph}{Przygotowanie}{7}% 
\contentsline {subsection}{\numberline {1.7}Polędwiczki z indyka w sosie porowo-śmietanowym}{8}% 
\contentsline {paragraph}{Składniki:}{8}% 
\contentsline {paragraph}{Przygotowanie}{8}% 
\contentsline {subsection}{\numberline {1.8}Sernik królewski}{9}% 
\contentsline {paragraph}{Składniki:}{9}% 
\contentsline {paragraph}{Przygotowanie}{9}% 
\contentsline {section}{\numberline {2}Desery}{10}% 
\contentsline {subsection}{\numberline {2.1}Ciasto czekoladowe}{10}% 
\contentsline {paragraph}{Składniki:}{10}% 
\contentsline {paragraph}{Przygotowanie}{10}% 
\contentsline {subsection}{\numberline {2.2}Kruche ciastka z marmoladą}{11}% 
\contentsline {paragraph}{Składniki:}{11}% 
\contentsline {paragraph}{Przygotowanie}{11}% 
\contentsline {subsection}{\numberline {2.3}Bezy}{12}% 
\contentsline {paragraph}{Składniki:}{12}% 
\contentsline {paragraph}{Przygotowanie}{12}% 
\contentsline {subsection}{\numberline {2.4}Gofry}{13}% 
\contentsline {paragraph}{Składniki:}{13}% 
\contentsline {paragraph}{Przygotowanie}{13}% 
\contentsline {subsection}{\numberline {2.5}Biszkopt z musem}{14}% 
\contentsline {paragraph}{Składniki:}{14}% 
\contentsline {paragraph}{Przygotowanie}{14}% 
\contentsline {paragraph}{Biszkopt}{14}% 
\contentsline {paragraph}{Krem}{15}% 
\contentsline {paragraph}{Mus truskawkowy}{15}% 
\contentsline {subsection}{\numberline {2.6}Muffiny z truskawkami}{16}% 
\contentsline {paragraph}{Składniki:}{16}% 
\contentsline {paragraph}{Przygotowanie}{16}% 
\contentsline {subsection}{\numberline {2.7}Szynka w sosie własnym}{17}% 
\contentsline {paragraph}{Składniki}{17}% 
\contentsline {paragraph}{Przygotowanie}{17}% 
